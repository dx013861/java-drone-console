package uk.ac.reading.dx013861;

import java.util.Random;



public enum Direction {

	    North,
	    South,
	    East,
	    West;

	
								//We use Enum as we are using a different type of data one which is a set of predefined constants 
									
	
	
	
	
	
	  
	    public static Direction getRandom(){
	        Random r = new Random();
	        return values()[r.nextInt(values().length)];
	    }

	    						//getRandom selects a random direction from the Direction values above
	    
	    
	    
	    
	    
	   
	    public Direction next(Direction direction) {
	        if(direction == Direction.North){
	            direction = Direction.East;
	        }else  if(direction == Direction.East){
	            direction = Direction.South;
	        }else if(direction == Direction.South){
	            direction = Direction.West;
	        }else {
	            direction = Direction.North;
	        }
	        					//This moves the direction clockwise for the next direction

	        return direction;
	    }


	    
	    
	    
	    
	    
	    						//The string is converted into direction
	    public static Direction fromString(String s){
	        s = s.trim().toLowerCase();
	        if(s.equals("north")){
	            return Direction.North;
	        } else if(s.equals("east")){
	            return Direction.East;
	        } else if(s.equals("south")){
	            return Direction.South;
	        } else if(s.equals("west")) {
	            return Direction.West;
	        }else{
	            return null;
	        }
	    }
	}
