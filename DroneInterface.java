package uk.ac.reading.dx013861;

import org.json.simple.JSONObject;
import java.util.Scanner;









class DroneInterface {

	    private Scanner s;						// scanner is used to read input data from the user
	    private DroneArena myArena;				// arena in which the drones will be shown in
	    private  ArenaStorage store;			// this will be where we store arena data
	    private Direction direction;
	 
	    
	    
	    
	  
	   
	    
	    public DroneInterface() {
	        s = new Scanner(System.in);			// set up scanner for the user to input
	        myArena = new DroneArena(10, 20);	// create arena size 10*20
	        store = new ArenaStorage(); 
	        
	        
	        
	        
	        
	        
	        
	        char ch = ' ';
	        do {
	            System.out.print("Enter (A)dd drone, get (I)nformation, (D)isplay Drones, (M)ove drones 10 times, (B)ild new arena, (S)ave arena, (L)oad arena or e(X)it > ");
	            ch = s.next().charAt(0);		// sets up interface
	            s.nextLine();
	            switch (ch) {
	                case 'A' :
	                case 'a' :
	                    myArena.AddDrone();		// adds a new drone to arena
	                    break;
	                case 'I' :
	                case 'i' :
	                    System.out.print(myArena.toString());
	                    break;
	                case 'x' : 	ch = 'X';		// when the input X is detected this will end the program
	                    break;
	                case 'd':
	                case 'D':
	                    this.Display();		// display the arena with the drone if they have been added
	                    break;
	                case 'm':
	                case 'M':
	                    try {
	                        xMoves(10);			// move all drones in the arena to move 10 positions
	                    } catch (InterruptedException e) {
	                        e.printStackTrace();
	                    }
	                    break;
	                case 'b':
	                case 'B':
	                    makeNewArena();		// constructs a new arena
	                    break;
	                case 's':
	                case 'S':
	                    StoreArena();			// stores the data of the arena in a place holder
	                    break;
	                case 'l':
	                case 'L':
	                    LoadArena();			// loads the relevant saved arena data
	                    break;
	                default:
	                    throw new IllegalStateException("Unexpected value: " + ch);
	            }
	        } while (ch != 'X');						

	        s.close();						// close scanner
	    }

	    
	    
	    
	    
	    
	    
	    
	   
	    
	    void LoadArena() {
	        Scanner sc = new Scanner(System.in);  				//Loads the saved arena from accessing the stored data
	        System.out.print("Enter the name for the arena: ");
	        String params = sc.nextLine();
	        JSONObject oj = store.readFromFile(params);
	        myArena = store.JsonToObject(oj);
	    }

	    
	    
	    
	    
	    
	    
	    
	    
	    
	    void StoreArena(){
	        try{
	            Scanner sc = new Scanner(System.in);  
	            System.out.print("Enter the file name for the arena: ");
	            String params = sc.nextLine();  
	            JSONObject jo = store.objectToJson(myArena);
	            if(jo != null){
	                store.writeToFile(jo, params.trim());
	            }else{
	                throw new Exception("Failed to store arena");
	            }
	        } catch (Exception ex){
	            ex.printStackTrace();
	        }
	    }
	    									//Saves and stores the arena details by storing the data which we can load later once it is called

	    
	    
	    
	    
	    
	    
	    
	    
	    
	    void xMoves(int times) throws InterruptedException {
	        for (int i = 0; i < times; i++) {
	            myArena.moveAllDrones(myArena);
	            Display();
	            Thread.sleep(200,1);
	        }
	    }

	    
	    
	    void Display() {
	        ConsoleCanvas c = new ConsoleCanvas(myArena.getsizX(), myArena.getsizY());
	        myArena.showDrones(c);
	        System.out.println(c.toString());
	    }

	    
	    
	    
	    public DroneArena makeNewArena(){
	        int x,y;
	        Scanner sc = new Scanner(System.in); 
	        System.out.print("Enter x,y for new building size (x,y): ");
	        String params = sc.nextLine(); 

	        try{
	        
	            String[] p1 = params.split(",");
	            x = Integer.parseInt(p1[0].trim());
	            y = Integer.parseInt(p1[1].trim());
	            if (x < 3 || y < 3) {
	                throw new Exception("Needs to be 3,3 or larger");
	            }
	            this.myArena = new DroneArena(x,y);
	            return myArena;
	        } catch (Exception ex){
	            System.out.println("value entered incorrectly, creating default size");
	            this.myArena = new DroneArena();
	            return myArena;
	        }
	    }

	    
	    
	    
	    public static void main(String[] args) {
	        DroneInterface r = new DroneInterface();
	    }

	}
