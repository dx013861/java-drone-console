package uk.ac.reading.dx013861;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;







public class DroneArena {
	
	
	
	    											
	    private int sizX, sizY;

	    
	    											
	    private List<Drone> drones = new ArrayList<Drone>();
	    
	    
	    
	    

	    public List<Drone> getDrones() {
	        return drones;
	    }

	    
	    
	    
	    
	    int getsizX() {
	        return sizX;
	    }

	    
	    
	    
	    
	    
	    int getsizY() {
	        return sizY;
	    }

	    
	    
	    
	    
	    
	    int getDronesCount() {
	        return drones.size();
	    }

	    						//Returns the number of current drones
	    
	    
	    
	    int maxPossibleDrones() {
	        return (getsizX()-2) * (getsizY()-2);
	    }

	    
	    
	    
	    
	    
	    boolean canAddDrone() {
	        return getDronesCount() < maxPossibleDrones();
	    }

	    						
	    
	    
	    
	    
	   
	    public DroneArena(){
	       new DroneArena(10,20);
	    }

	    					//Creates an arena of size 10*20
	    
	    
	    
	    				
	    public DroneArena(int x, int y){
	        this.sizX = x;
	        this.sizY = y;
	    }
	    				//Function allows the constructor to override current arena with specified size
	    
	    
	    
	    
	    
	    				
	    public void AddDroneMiddle(){
	        Drone d1 = new Drone(this.sizX/2, this.sizY/2, Direction.getRandom());
	        drones.add(d1);
	    }
	    				//Function creates a new drone in the centre of arena
	    
	    
	    
	    
	    
	    
	    
	    void AddDrone(){
	        Random r1 = new Random();
	        int xPosition = r1.nextInt(this.sizX)+1;
	        int yPosition = r1.nextInt(this.sizY)+1;

	        
	        
	        AddDrone(xPosition, yPosition, Direction.getRandom());
	    }

	    					//AddDrone function adds a drone in a random position within the arena
	    
	    
	    

	    void AddDrone(int x, int y, Direction d){
	        
	        if(getDroneAt(x, y) == null && x < this.sizX-1 && y < this.sizY-1){
	            drones.add(new Drone(x, y, d));
	        }else{
	            try{
	                if(canAddDrone()){
	                    AddDrone();
	                }else{
	                    throw new Exception("Arena is full!");
	                }
	            }catch (Exception ex){
	                System.out.println(ex.getMessage());
	            }
	        }
	    }
	    					//Checks if the number of drones in arena is at its maximum and if it is then it prints out a message
	    
	    
	    
	    
	    

	    public String toString(){
	        String s;
	        s = "The drone area is "+this.sizX+","+this.sizY;
	        for (int i = 0; i <drones.size() ; i++) {
	            s+= "\n\t"+drones.get(i).toString();
	        }
	        s+="\n";

	        return s;
	    }

	    
	    
	    
	    
	    
	    private Drone getDroneAt(int x, int y){
	        for (Drone n:drones) {
	            if(n.checkDrone(x,y)){
	                return n;
	            }
	        }
	        return null;
	    }

	    
	    
	    
	    

	    void showDrones(ConsoleCanvas c){
	        for (Drone d:drones) {
	            d.displayDrone(c);
	        }
	    }

	    
	    
	    
	    
	    
	    

	    boolean canMoveHere(int x, int y){

	    	
	    	
	    	
	    	
	    	
	        return x < this.sizX - 1 && y < this.sizY - 1
	                && x != 0 && y != 0
	                && getDroneAt(x, y) == null;
	    }

	    
	    
	
	    
	    
	    

	    void moveAllDrones(DroneArena area){
	        for (Drone d:drones) {
	           d.tryToMove(this);
	        }
	    }
	    
	    
	    
	    
	    public void moveAllDrones(DroneArena area, int times){
	        for (Drone d:drones) {
	            for (int i = 0; i < times; i++) {
	                try{
	                    d.tryToMove(this);
	                }catch (Exception es){
	                    es.printStackTrace();
	                }
	            }
	        }							//Moves all drones n times once it is called from DroneInterface along with an input in this case 10
	    }
	}
