package uk.ac.reading.dx013861;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;






public class ArenaStorage {

	
	
	    							
	    JSONObject objectToJson(DroneArena d){
	        JSONObject obj = new JSONObject();
	        obj.put("areaX", d.getsizX());
	        obj.put("areaY", d.getsizY());

	        JSONArray arr = new JSONArray();
	        
	        						//A Json object is created from converting a drone area object
	        
	        
	        		
	        for (int i = 0; i < d.getDronesCount(); i++) {
	            JSONObject droneObj = new JSONObject();
	            try{
	                Drone tmpDrone = d.getDrones().get(i);
	                droneObj.put("id", tmpDrone.getID());
	                droneObj.put("xPos", tmpDrone.getPositionX());
	                droneObj.put("yPos", tmpDrone.getPositionY());
	                droneObj.put("direction", tmpDrone.getDirection().toString());

	                
	                
	                
	                
	                
	                				
	                arr.add(i, droneObj);
	            }catch (Exception ex){
	                ex.printStackTrace();
	                return null;
	            }
	            					//Adds drone object to drone's array
	            
	            
	            
	            					
	            obj.put("Drones",arr);
	        }
	        						//Then adds all present drones into a json object
	        
	        
	        return obj;
	    }
	    
	    
	    
	    

	    							
	    DroneArena JsonToObject(JSONObject jo){
	        DroneArena d1;
	        try{
	            Long x = (Long) jo.get("areaX");
	            Long y = (Long) jo.get("areaY");

	            					//We take the Json file and converts it to "DroneArea" with all drones
	            
	            
	            
	        
	            d1 = new DroneArena(x.intValue(),y.intValue());

	            JSONArray drones = (JSONArray) jo.get("Drones");
	            
	            
	            
	            
	            					
	            for (int i = 0; i <drones.size() ; i++) {
	                JSONObject droneObj = (JSONObject) drones.get(i);
	                Long xPos = (Long) droneObj.get("xPos");
	                Long yPos = (Long) droneObj.get("yPos");
	                String dir = (String) droneObj.get("direction");
	                Direction d = Direction.fromString(dir);

	                
	                
	                
	                
	                	
	                d1.AddDrone(xPos.intValue(), yPos.intValue(), d);
	            }
	        }catch (Exception ex) {
	            System.out.println(ex.getMessage());
	            return null;
	        }
	        System.out.println("Loaded");
	        return d1;
	    }

	    
	    
	    
	    
	    							//Writes the Json file into the suitable working inventory so it is accessible later
	    void writeToFile(JSONObject js, String fileName){
	        try{
	            fileName+=".txt";
	            FileWriter f = new FileWriter(fileName);
	            f.write(js.toJSONString());
	            f.flush();
	            System.out.println("Saved");
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }

	    
	    
	    
	    
	    							// reads json object from specified file
	    JSONObject readFromFile(String file){
	        JSONParser parser = new JSONParser();
	        try{
	            Object ob1 = parser.parse(new FileReader(file+".txt"));
	            JSONObject oj = (JSONObject) ob1;
	            return oj;
	        }catch (Exception ex){
	            ex.printStackTrace();
	            return null;
	        }
	    }
	}
