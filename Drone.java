package uk.ac.reading.dx013861;

import javax.swing.*;
import java.util.Random;





public class Drone {
	
	
	    private int positionX;
	    
	    private int positionY;				
	    
	    private int ID;
	    
	    private static int UID;
	    
	    Direction direction;
	    
	    
	    

	    public int getPositionX() {
	        return positionX;
	    }

	    
	    
	    
	    
	    
	    public void setPositionX(int positionX) {
	        this.positionX = positionX;
	    }

	    
	    
	    
	    
	    
	    public int getPositionY() {
	        return positionY;
	    }

	    
	    
	    
	    
	    
	    public void setPositionY(int positionY) {
	        this.positionY = positionY;
	    }

	    
	    
	    
	    
	    public int getID() {
	        return ID;
	    }

	    
	    
	    
	    
	    
	    public void setID(int ID) {
	        this.ID = ID;
	    }

	    
	    
	    
	    
	    public static int getUID() {
	        return UID;
	    }

	    
	    
	    
	    
	    public static void setUID(int UID) {
	        Drone.UID = UID;
	    }

	    
	    
	    
	    
	    public Direction getDirection() {
	        return direction;
	    }

	    
	    
	    
	    
	    public void setDirection(Direction direction) {
	        this.direction = direction;
	    }

	    
	    
	    
	    
	    										//Drone constructor
	    public Drone(int x, int y, Direction d){
	        this.positionX = x;
	        this.positionY = y;
	        this.direction = d;
	        UID++;
	        this.ID = UID;
	    }
	    										//States unique identifier with ID
	    
	    
	    
	    
	    public Drone(){
	    }

	    										//Checks to see if there is a drone in current location
	    boolean checkDrone(int x, int y){
	        return this.positionX == x && this.positionY == y;
	    }

	    
	    
	    						
	    public String toString(){
	        return "Drone " + this.ID +
	                " is at " + this.positionX +","+this.positionY+
	                " and is facing " +this.direction;
	    }

	    										//Prints out in interface whether there is a drone present along with positions
	    
	    
	
	    void displayDrone(ConsoleCanvas c){
	        c.showIt(this.positionX, this.positionY, "d");
	    }										//Displays the position of the drone in the arena using the letter "d"

	    
	    
	    boolean tryToMove(DroneArena area){
	        int nx = this.positionX;
	        int ny = this.positionY;

	        if(this.direction == Direction.North){
	            ny++;
	        }else if(this.direction == Direction.East){
	            nx++;
	        }else if(this.direction == Direction.South){
	            ny--;
	        }else if(this.direction == Direction.West){
	            nx--;
	        }

	        
	        
	        
	        									
	        if(area.canMoveHere(nx, ny)){
	        	
	            								//This moves the drone to a new position
	            this.positionX = nx;
	            this.positionY = ny;
	            return true;
	        }else{
	        	
	            								//If the drone can't be moved then it turns the direction around by one
	            this.direction = direction.next(this.direction);
	            return false;
	        }
	    }
	}

